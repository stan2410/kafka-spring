create table kafka
(
    id          SERIAL PRIMARY KEY,
    message       VARCHAR(255) NOT NULL,
    create_date TIMESTAMP    NOT NULL
);