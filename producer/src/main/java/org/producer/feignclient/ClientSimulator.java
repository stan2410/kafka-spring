package org.producer.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "feignClient", url = "${consumerApi}")
public interface ClientSimulator {

    @GetMapping
    public void sendRequest(@RequestParam("message") String message);
}
