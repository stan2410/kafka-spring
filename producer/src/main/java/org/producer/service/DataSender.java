package org.producer.service;

public interface DataSender {

    void send(String message);
}
