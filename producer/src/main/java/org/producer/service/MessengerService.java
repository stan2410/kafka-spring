package org.producer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class MessengerService {

    @Value("${target}")
    private String target;

    private final Map<String, DataSender> clients;

    public MessengerService(Map<String, DataSender> clients) {
        this.clients = clients;
    }

    public void sendRandomString(String random) {
        try {
            log.info("Вхожу в DataSender");
            var client = clients.get(target);
            client.send(random);
        } catch (RuntimeException ex) {
            log.error("Target doesn't exist", ex);
        }
    }
}
