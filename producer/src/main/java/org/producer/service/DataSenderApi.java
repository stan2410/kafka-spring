package org.producer.service;

import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import org.producer.feignclient.ClientSimulator;
import org.springframework.stereotype.Service;

@Service("api")
@Slf4j
public class DataSenderApi implements DataSender {
    private final ClientSimulator clientSimulator;

    public DataSenderApi(ClientSimulator clientSimulator) {
        this.clientSimulator = clientSimulator;
    }

    @Override
    public void send(String message) {
        try {
            log.info("Отправляю сообщение в DataSenderApi");
            clientSimulator.sendRequest(message);
            log.info("Сообщение: {} было отправлено", message);
        } catch (RetryableException e) {
            log.error("Потребитель не отвечает \n{}", e.getMessage());
        }
    }
}
