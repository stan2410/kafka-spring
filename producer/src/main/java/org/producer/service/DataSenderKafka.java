package org.producer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service("kafka")
@Slf4j
public class DataSenderKafka implements DataSender {

    @Value("${application.kafka.topic}")
    private String topicName;

    private final KafkaTemplate<String, String> template;

    public DataSenderKafka(KafkaTemplate<String, String> template) {
        this.template = template;
    }

    @Override
    public void send(String message) {
        try {
            log.info("Отправляю сообщение в DataSenderKafka");
            template.send(topicName, message).addCallback(new ListenableFutureCallback<>() {

                @Override
                public void onFailure(Throwable e) {
                    log.error("message:{} was not sent", message, e);
                }

                @Override
                public void onSuccess(SendResult<String, String> result) {
                    log.info("message:{} was sent, offset:{}", message, result.getRecordMetadata().offset());
                }
            });
        } catch (Exception e) {
            log.error("send error, value:{}", message, e);
        }
    }
}
