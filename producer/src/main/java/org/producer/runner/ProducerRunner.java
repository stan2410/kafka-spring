package org.producer.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ProducerRunner implements ApplicationRunner {
    private final RequestGenerator requestGenerator;

    public ProducerRunner(RequestGenerator requestGenerator) {
        this.requestGenerator = requestGenerator;
    }

    @Override
    public void run(ApplicationArguments args) {
        requestGenerator.generate();
    }
}