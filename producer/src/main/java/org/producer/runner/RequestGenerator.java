package org.producer.runner;

import lombok.extern.slf4j.Slf4j;
import org.producer.service.MessengerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class RequestGenerator {

    @Value("${period}")
    private int period;

    private final MessengerService messengerService;

    public RequestGenerator(MessengerService messengerService) {
        this.messengerService = messengerService;
    }

    public void generate() {
        var executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(() -> messengerService.sendRandomString(generateRandomString()),
                0, period, TimeUnit.SECONDS);
        log.info("START MESSAGING");
    }

    private String generateRandomString() {
        Random random = new Random();
        char[] arr = new char[random.nextInt(100)];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (char) ('a' + random.nextInt(26));
        }
        return new String(arr);
    }
}