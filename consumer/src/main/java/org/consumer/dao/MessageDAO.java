package org.consumer.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

@Service
@Slf4j
public class MessageDAO {
    private final DataSource dataSource;

    public MessageDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Transactional
    public String add(String value) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "insert into kafka (message, create_date) values (?,?)")) {
            preparedStatement.setString(1, value);
            preparedStatement.setTimestamp(2, Timestamp.from(Instant.now()));
            var resultSet = preparedStatement.executeUpdate();
            if (resultSet == 1) {
                return String.format("message: %s was recorded", value);
            }
        } catch (SQLException e) {
            log.error("message:{} not recorded \n{}", value, e.getMessage());
        }
        return "message not recorded";
    }
}