package org.consumer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaListenerService {
    private final IncomingMessageService messageService;

    public KafkaListenerService(IncomingMessageService messageService) {
        this.messageService = messageService;
    }

    @KafkaListener(topics = "${application.kafka.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void messageRequestListen(String message) {
        var answer = messageService.save(message);
        log.info("{}", answer);
    }
}
