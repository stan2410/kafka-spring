package org.consumer.service;

import lombok.extern.slf4j.Slf4j;
import org.consumer.dao.MessageDAO;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class IncomingMessageService {
    private final MessageDAO messageDAO;

    public IncomingMessageService(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    public String save(String message) {
        var answer = messageDAO.add(message);
        log.info("{}", answer);
        return answer;
    }
}
