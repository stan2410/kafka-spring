package org.consumer.controller;

import lombok.RequiredArgsConstructor;
import org.consumer.service.IncomingMessageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ConsumerController {
    private final IncomingMessageService service;

    @GetMapping("/send")
    public void incoming(@RequestParam("message") String message) {
        service.save(message);
    }
}